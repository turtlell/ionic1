// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
app = angular.module('starter', ['ionic', 'oc.lazyLoad']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('home', {
            url: '/home',
            templateUrl: 'templates/home.html',
            controller: 'HomeCtrl',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/controllers/home.js')
                }],
                loadMyService: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/services/home.js')
                }]
            }
        })
        .state('more', {
            url: '/more',
            templateUrl: 'templates/more.html',
            controller: 'MoreCtrl',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/controllers/more.js')
                }],
                loadMyService: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/services/more.js')
                }]
            }
        })
        .state('house', {
            url: '/house',
            templateUrl: 'templates/house.html',
            controller: 'HouseCtrl',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/controllers/house.js')
                }],
                loadMyService: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/services/house.js')
                }],

            }
        })
        .state('city', {
            url: '/city',
            templateUrl: 'templates/city.html',
            controller: 'CityCtrl',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/controllers/city.js')
                }],
                loadMyService: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/services/city.js')
                }]
            }
        })
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/controllers/login.js')
                }],
                loadMyService: ['$ocLazyLoad', function($ocLazyLoad){
                    return $ocLazyLoad.load('js/services/login.js')
                }]
            }
        })


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/home');

})
