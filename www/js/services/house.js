angular.module('starter')
.factory("HouseService", function () {
    return{
        initMap: function(){
            var map = new BMap.Map("housemap");    // 创建Map实例
            var point = new BMap.Point(115.92393921534, 28.691266169569);
            map.centerAndZoom(point, 15);   //地图初始化，后面数字为zoom级别
            var marker = new BMap.Marker(point);        // 创建标注
            map.addOverlay(marker);                     // 将标注添加到地图中
            marker.setAnimation(BMAP_ANIMATION_BOUNCE);
            //向地图中添加缩放控件
            var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
            map.addControl(ctrl_nav);
            //向地图中添加比例尺控件
            var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
            map.addControl(ctrl_sca);
            var circle = new BMap.Circle(point,1000,{fillColor:"blue", strokeWeight: 1 ,fillOpacity: 0.3, strokeOpacity: 0.3});
            map.addOverlay(circle);
            var local =  new BMap.LocalSearch(map, {renderOptions: {map: map, autoViewport: false}});
            $(function(){
                local.searchNearby('公交',point,1000);
                $('#bus').addClass('chosed');
                $('#subway,#restaurant').removeClass('chosed');
            }); 
            $('#bus').click(function(){
                local.searchNearby('公交',point,1000);
                $('#bus').addClass('chosed');
                $('#subway,#restaurant').removeClass('chosed');
            }); 
            $('#subway').click(function(){
                local.searchNearby('地铁',point,1000);
                $('#subway').addClass('chosed');
                $('#bus,#restaurant').removeClass('chosed');
            }); 
            $('#restaurant').click(function(){
                local.searchNearby('餐饮',point,1000);
                $('#restaurant').addClass('chosed');
                $('#subway,#bus').removeClass('chosed');
            }); 
        }   
    }

});
