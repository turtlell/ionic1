angular.module('starter')
.controller('MoreCtrl', function($scope, $timeout) {
    // 初始化各值
    $scope.tagToggle = true
    $scope.districtToggle = true;
    $scope.priceToggle = true;
    $scope.areaToggle = true;
    $scope.metroToggle = true;
    $scope.oneLineToggle = false;
    $scope.twoLineToggle = false;
    $scope.backDrop = false;
    // 上拉刷新
    $scope.flag = true;
    $scope.loadEnd = '数据加载完成';
    $scope.items = []; 
    var base = 0;
    $scope.loadMore = function(){
        $timeout(function(){
            for(var i=0;i<5;i++,base++){
                if(base>10){
                    $scope.flag = false;
                    break;
                }   
                $scope.items.push(base+1);
            }   

            $scope.$broadcast("scroll.infiniteScrollComplete");
        },500);
    };
    // 下拉刷新
    $scope.doRefresh = function() {
        location.reload();
        $scope.$broadcast("scroll.refreshComplete");
    };  

    // 标签筛选
    $scope.toggleTag = function(myStatus) {
        if(!$scope[myStatus]){
            $scope[myStatus] = !$scope[myStatus];
            $scope.backDrop = false;
        }else{
            $scope.tagToggle = true;
            $scope.districtToggle = true;
            $scope.priceToggle = true;
            $scope.areaToggle = true;
            $scope.backDrop = true;
            $scope[myStatus] = !$scope[myStatus];
        }
    }
});
